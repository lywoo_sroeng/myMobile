import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

// NOTE
// ====

// -  Update value of state, it re-render a new value to view automatically
// -  export class Timer    =>    import { Timer } from './components/timer'
// -  export default class Timer    =>    import Timer from './components/timer'

export class Timer extends Component{
  constructor(props){
    super(props);
    this.state = {
      hr: 0,
      mn: 0,
      s: 0
    };

    console.log('constructor');    
  }

  render() {
    console.log('on render');
    return (
      <View>
        <Text style={styles.textCenter}>Timer of props: {this.props.hr}:{this.props.mn}:{this.props.s}</Text>
        <Text style={styles.textCenter}>Timer of state: {this.state.hr}:{this.state.mn}:{this.state.s}</Text>
        <Text style={styles.textCenter}>new prop of state: {this.state.new_state_prop}</Text>
      </View>
    )
  }

  componentDidMount() {
    // ***  Function within it work asynchronously  ***

    // good for AJAX: fetch, ajax, or subscriptions.
  
    // invoked once (client-side only).
    // fires before initial 'render'

    console.log('on componentDidMount');
    
    this.setState(() => {
      return { hr: this.props.hr, mn: this.props.mn, s: parseInt(this.state.s) + 1 , new_state_prop: 'new state prop'};
      // Update state prop value and also add one more state prop
    });    

    console.log(this.state);  //  output: {hr: 0, mn: 0, s: 0}
    
  }
}

const styles = StyleSheet.create({
  textCenter: {textAlign: 'center'}
})